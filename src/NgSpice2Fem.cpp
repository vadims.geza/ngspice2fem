#include "NgSpice2Fem.h"

#include <stdexcept>
#include <windows.h> 
#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <armadillo>
#include "DataAnalyzer.h"


using namespace std;

VecNames NgSpice2Fem::vec_names = VecNames();

NgSpice2Fem::NgSpice2Fem() :
    ngSpice_Init_call(nullptr),
    ngSpice_Circ_call(nullptr),
    ngSpice_Command_call(nullptr),
    ngGet_Vec_Info_call(nullptr),
    ngSpice_CurPlot_call(nullptr),
    ngSpice_AllPlots_call(nullptr),
    ngSpice_AllVecs_call(nullptr),
    ngSpice_Running_call(nullptr)
{
}

NgSpice2Fem::~NgSpice2Fem()
{
}

void NgSpice2Fem::SetLibrary(string path)
{
    dll_path = path;
}

void NgSpice2Fem::Initialize()
{
    HINSTANCE hinstLib = LoadLibraryA(dll_path.c_str());
    if (hinstLib != NULL)
    {
        // get all function pointers
        ngSpice_Init_call = (ngSpice_Init)GetProcAddress(hinstLib, "ngSpice_Init");
        ngSpice_Circ_call = (ngSpice_Circ)GetProcAddress(hinstLib, "ngSpice_Circ");
        ngSpice_Command_call = (ngSpice_Command)GetProcAddress(hinstLib, "ngSpice_Command");
        ngGet_Vec_Info_call = (ngGet_Vec_Info)GetProcAddress(hinstLib, "ngGet_Vec_Info");
        ngSpice_CurPlot_call = (ngSpice_CurPlot)GetProcAddress(hinstLib, "ngSpice_CurPlot");
        ngSpice_AllPlots_call = (ngSpice_AllPlots)GetProcAddress(hinstLib, "ngSpice_AllPlots");
        ngSpice_AllVecs_call = (ngSpice_AllVecs)GetProcAddress(hinstLib, "ngSpice_AllVecs");
        ngSpice_Running_call = (ngSpice_Running)GetProcAddress(hinstLib, "ngSpice_running");
    }
    else
    {
        cout << " Could not initalize NgSpice2Fem! Cannot load library. " << endl;
        return;
    }


    ngSpice_Init_call(&NgSpice2Fem::CallSendChar, &NgSpice2Fem::CallSendStat, 
        &NgSpice2Fem::CallControlledExit, NULL, 
        NULL, NULL, this);
}

void NgSpice2Fem::LoadNetlist(std::string netlist_file)
{
    std::ifstream file(netlist_file);
    std::stringstream buffer;
    if (file)
    {
        buffer << file.rdbuf();
        file.close();
    }
    else
    {
        cout << "Could not load netlist file" << endl;
        return;
    }

    // ngspice works correctly only with C locale
    vector<char*> lines;
    netlist = "";
    while (!buffer.eof())
    {
        char line[1024];
        buffer.getline(line, sizeof(line));
        lines.push_back(strdup(line));
        netlist += std::string(line) + std::string("\n"); 
    }
    
    lines.push_back(nullptr); // sentinel, as requested in ngSpice_Circ description

    ngSpice_Circ_call(lines.data());
}

void NgSpice2Fem::SetInputVariables(std::vector<InputValue> inp)
{
    input_values = inp;
}

void NgSpice2Fem::SetOuputVariables(std::vector<OutputValue> out)
{
    output_values = out;
}

void NgSpice2Fem::Run()
{
    for (auto inp : input_values)
    {
        std::string command = "alterparam " + inp.name + " = " +  std::to_string(inp.value);
        char* cmd = const_cast<char*>(command.c_str());
        ngSpice_Command_call(cmd);

    }

    ngSpice_Command_call("reset");
    bool success = ngSpice_Command_call("bg_run");     // bg_* commands execute in a separate thread

    if (success)
    {
        do
        {
            Sleep(50);
        } while (ngSpice_Running_call());
    }

    Sleep(50);
    ngSpice_Command_call("bg_run");

    do
    {
        Sleep(50);
    } while (ngSpice_Running_call());

}

double* NgSpice2Fem::GetOutput()
{
    char* current_plot = ngSpice_CurPlot_call();
    char** all_vecs = ngSpice_AllVecs_call(current_plot);
    vector_info* time_info = ngGet_Vec_Info_call("time");

    std::vector<double> time_vec;
    time_vec.reserve((*time_info).v_length);
    for (int j = 0; j < (*time_info).v_length; j++)
    {
        time_vec.push_back((*time_info).v_realdata[j]);
    }

    double*  return_arr = new double[output_values.size()];

    for (int i = 0; i< output_values.size(); i++)
    {
        char* nm = const_cast<char*>(output_values[i].name.c_str());
        vector_info* vi = ngGet_Vec_Info_call(nm);
        DataAnalyzer da(vi, time_vec);

        double val;
        if (output_values[i].type == "FREQUENCY")
        {
            val = da.getFrequency();
            std::cout << "Frequency " << val << std::endl;
        }
        else if (output_values[i].type == "RMS")
        {
            val = da.getRMS();
            std::cout << "RMS " << val << std::endl;
        }
        else if (output_values[i].type == "AMPLITUDE")
        {
            val = da.getAmplitude();
            std::cout << "Amplitude " << val << std::endl;
        }
        else if (output_values[i].type == "MEAN")
        {
            val = da.getMean();
            std::cout << "Mean " << val << std::endl;
        }
        return_arr[i] = val;
    }

    return return_arr;
}


int NgSpice2Fem::CallSendChar(char* s, int id, void* user)
{
    string str = s;
    
    // strip stdout/stderr from the line
    if ((boost::iequals(str.substr(0, 7), "stdout "))
            || (boost::iequals(str.substr(0, 7), "stderr ")))
            s += 7;
 //   std::cout << s << std::endl;
    return 0;
}

int NgSpice2Fem::CallSendStat(char* s, int id, void* user)
{
    return 0;
}

int NgSpice2Fem::CallControlledExit(int status, bool immediate, bool exit_on_quit, int id, void* user)
{
    return 0;
}

int  NgSpice2Fem::CallSendData(pvecvaluesall vecs, int nr_of_vectors, int id, void* user)
{
    return 0;
}
        
int  NgSpice2Fem::CallSendInitData(pvecinfoall data, int id, void* user)
{
    return 0;
}
