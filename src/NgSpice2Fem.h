
#pragma once

#include <ngspice/sharedspice.h>
#include <string>
#include <iostream>
#include <vector>
#include "InputReader.h"

class VecNames;

class NgSpice2Fem
{
public:
    NgSpice2Fem();
    ~NgSpice2Fem();

    void SetLibrary(std::string);

    void Initialize();

    void LoadNetlist(std::string);

    void SetInputVariables(std::vector<InputValue>);
    void SetOuputVariables(std::vector<OutputValue>);


    void Run();
    double* GetOutput();

private:
    // ngspice functions
    typedef void (*ngSpice_Init)(SendChar*, SendStat*, ControlledExit*, SendData*, SendInitData*, BGThreadRunning*, void*);
    typedef int (*ngSpice_Circ)(char** circarray);
    typedef int (*ngSpice_Command)(char* command);
    typedef pvector_info(*ngGet_Vec_Info)(char* vecname);
    typedef char* (*ngSpice_CurPlot)(void);
    typedef char** (*ngSpice_AllPlots)(void);
    typedef char** (*ngSpice_AllVecs)(char* plotname);
    typedef bool (*ngSpice_Running)(void);

    ///< Handle to DLL functions
    ngSpice_Init ngSpice_Init_call;
    ngSpice_Circ ngSpice_Circ_call;
    ngSpice_Command ngSpice_Command_call;
    ngGet_Vec_Info ngGet_Vec_Info_call;
    ngSpice_CurPlot  ngSpice_CurPlot_call;
    ngSpice_AllPlots ngSpice_AllPlots_call;
    ngSpice_AllVecs ngSpice_AllVecs_call;
    ngSpice_Running ngSpice_Running_call;

    std::string dll_path;

    std::string netlist;
    std::vector<InputValue> input_values;
    std::vector<OutputValue> output_values;
    bool is_initialized;
    static VecNames vec_names;
    static int  CallSendChar(char*, int, void*);
    static int  CallSendStat(char*, int, void*);
    static int  CallControlledExit(int, bool, bool, int, void*);
    static int  CallSendData(pvecvaluesall, int, int, void*);
    static int  CallSendInitData(pvecinfoall, int, void*);


};




class VecNames : public std::vector<std::string>
{
public:
    VecNames()
    {
        ;
    }

    VecNames(pvecvaluesall vecs)
    {

        for (int i = 0; i < vecs->veccount; i++)
        {
            this->push_back(vecs->vecsa[i]->name);
        }
    }

};