#include "DataAnalyzer.h"


DataAnalyzer::DataAnalyzer(vector_info* d, std::vector<double> tv)
{
    data = *d;
    std::vector<double> dv;
    dv.reserve(data.v_length);
    for (int j = 0; j < data.v_length; j++)
    {
        dv.push_back(data.v_realdata[j]);
    }

    //resample
    int size = 16384;
    if (data.v_length < size)
        size = data.v_length;

    vec t_inp(tv);
    vec data_inp(dv);

    vec t_out = linspace<vec>(0, tv.back(), size);

    vec data_out;

    arma::interp1(t_inp, data_inp, t_out, data_out, "*linear");

    data_vec = data_out;
    time_vec = t_out;
}

double DataAnalyzer::getFrequency()
{
    int fft_size = data_vec.size();

    arma::cx_vec  d = fft(data_vec, fft_size);

    double dt = time_vec.at(1) - time_vec.at(0);

    double Fs = 1 / (dt * fft_size);
    double freq;
    if (d.index_max() > fft_size / 2)
    {
        freq = (fft_size - d.index_max()) * Fs;
    }
    else
    {
        freq = d.index_max() * Fs;
    }

	return freq;
}

double DataAnalyzer::getMean()
{
    vec v(data_vec);
    return mean(v);
}

double DataAnalyzer::getRMS()
{
    vec v(data_vec);
    return stddev(v);
}

double DataAnalyzer::getAmplitude()
{
    vec v(data_vec);
    return sqrt(2) * stddev(v);
}
