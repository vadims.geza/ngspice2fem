#include "InputReader.h"
#include <iostream>


InputReader::InputReader(json inp)
{
	input = inp;
}

std::vector<InputValue> InputReader::getInputValues()
{
	if (input.find("input_val") == input.end())
	{
		std::cerr << "Value for 'input_val' not provided in input " << std::endl;
	}
	try
	{
		std::vector<InputValue> val;

		for (auto j : input.at("input_val").items())
		{
			InputValue v(j.key(), j.value());
			val.push_back(v);
		}
		return val;
	}
	catch (std::exception& e)
	{
		std::cerr << "Could not read 'input_val' value in input, please check input format!" << std::endl;
		std::exit(0);
	}
}

std::string InputReader::getLibraryPath()
{
	if (input.find("ngspice_dll") == input.end())
	{
		std::cerr << "Value for 'ngspice_dll' not provided in input " << std::endl;
	}
	try
	{
		std::string val = input.at("ngspice_dll").get<std::string>();
		return val;
	}
	catch (std::exception& e)
	{
		std::cerr << "Could not read 'ngspice_dll' value in input, please check input format!" << std::endl;
		std::exit(0);
	}
}

std::string InputReader::getNetList()
{
	if (input.find("circuit") == input.end())
	{
		std::cerr << "Value for 'circuit' not provided in input " << std::endl;
	}
	try
	{
		std::string val = input.at("circuit").get<std::string>();
		return val;
	}
	catch (std::exception& e)
	{
		std::cerr << "Could not read 'circuit' value in input, please check input format!" << std::endl;
		std::exit(0);
	}
}


std::vector<OutputValue> InputReader::getOutputValues()
{
	if (input.find("output_val") == input.end())
	{
		std::cerr << "Value for 'output_val' not provided in input " << std::endl;
	}
	try
	{
		std::vector<OutputValue> val;

		for (auto j : input.at("output_val").items())
		{
			OutputValue v(j.key(), j.value()["name"].get<std::string>(), j.value()["type"].get<std::string>());
			val.push_back(v);
		}
		return val;
	}
	catch (std::exception& e)
	{
		std::cerr << "Could not read 'output_val' value in input, please check input format!" << std::endl;
		std::exit(0);
	}
}