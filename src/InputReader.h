#pragma once

#include <vector>
#include "../third-party/json/json.hpp"

using json = nlohmann::json;


struct InputValue
{
	InputValue(std::string s, double d) : name(s), value(d) {};
	
	std::string name;
	double value;
};

struct OutputValue
{
	OutputValue(std::string k, std::string s, std::string t) : key(k), name(s), type(t) { value = 0; };

	std::string key;
	std::string name;
	std::string type;
	double value;
};


class InputReader
{
public:
	InputReader(json);

	std::vector<InputValue> getInputValues();
	std::vector<OutputValue> getOutputValues();
	std::string getLibraryPath();
	std::string getNetList();

private:
	json input;

};
