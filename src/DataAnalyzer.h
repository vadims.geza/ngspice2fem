#pragma once 

#include <ngspice/sharedspice.h>
#include <vector>
#include <armadillo>

using namespace arma;

class DataAnalyzer
{
public:
	DataAnalyzer(vector_info*, std::vector<double>);


	double getFrequency();
	double getMean();
	double getAmplitude();
	double getRMS();


	vector_info data;
	vec data_vec;
	vec time_vec;
};