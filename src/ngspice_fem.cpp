﻿// ngspice_fem.cpp : Defines the entry point for the application.
//

#include "ngspice_fem.h"
#include "NgSpice2Fem.h"	
#include "InputReader.h"
#include "../third-party/json/json.hpp"

using json = nlohmann::json;
using namespace std;


int main()
{
	json input;
	input["circuit"] = "D:/ngspice/ac_circuit.cir";
	input["ngspice_dll"] = "D:/ngspice/Spice64/bin-dll/ngspice.dll";

	input["input_val"]["res1"] = 1000;
	input["input_val"]["l1"] = 1e-6;
	input["input_val"]["freq"] = 6000;

	input["output_val"]["value1"]["name"] = "@r1[i]";
	input["output_val"]["value1"]["type"] = "RMS";

	input["output_val"]["value2"]["name"] = "@rshunt1[i]";
	input["output_val"]["value2"]["type"] = "AMPLITUDE";

	input["output_val"]["value3"]["name"] = "@rshunt1[i]";
	input["output_val"]["value3"]["type"] = "FREQUENCY";

	InputReader reader(input);
	NgSpice2Fem ng;

	ng.SetLibrary(reader.getLibraryPath());
	ng.Initialize();
	ng.LoadNetlist(reader.getNetList());
	ng.SetInputVariables(reader.getInputValues());
	ng.SetOuputVariables(reader.getOutputValues());

	ng.Run();

	double* output = ng.GetOutput();

	std::cout << "SUCCESS " << std::endl;
	return 0;
}





NGSPICE_API double* run(char* inp)
{
	json input = nlohmann::json::parse(inp);

	InputReader reader(input);

	NgSpice2Fem ng;

	ng.SetLibrary(reader.getLibraryPath());
	ng.Initialize();
	ng.LoadNetlist(reader.getNetList());
	ng.SetInputVariables(reader.getInputValues());
	ng.SetOuputVariables(reader.getOutputValues());

	ng.Run();

	double* output = ng.GetOutput();

	return output;
}
